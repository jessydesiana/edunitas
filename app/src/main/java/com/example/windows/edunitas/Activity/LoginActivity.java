package com.example.windows.edunitas.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.windows.edunitas.Pojo.Notif;
import com.example.windows.edunitas.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {

    FirebaseAuth mAuth;
    EditText mEmail, mPassword;
    Button mLogin;
    TextView tvRegister;
    FirebaseDatabase mDatabase;
    DatabaseReference mRoot, mNotifRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();
        if (mAuth !=null){
            startActivity(new Intent(LoginActivity.this, BerandaActivity.class));
        }

        mDatabase = FirebaseDatabase.getInstance();
        mRoot = mDatabase.getReference();
        mNotifRef = mRoot.child("notifications");

        mEmail = findViewById(R.id.etEmail);
        mPassword = findViewById(R.id.etPassword);
        mLogin = findViewById(R.id.btnLogin);
        tvRegister = findViewById(R.id.tvRegisterLogin);

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),RegisterActivity.class));
                finish();
            }
        });

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = mEmail.getText().toString();
                String password = mPassword.getText().toString();
                mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            registerToken();
                            startActivity(new Intent(LoginActivity.this,BerandaActivity.class));
                            finish();
                        } else {
                            Toast.makeText(LoginActivity.this, task.getException().getLocalizedMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
    }

    private void registerToken(){
        String userId = mAuth.getCurrentUser().getUid();
        String userToken = FirebaseInstanceId.getInstance().getToken();
        Notif notif = new Notif(userId,userToken, "");
        mNotifRef.child(userId).setValue(notif);
        Log.d("LoginAct","Token : " + userToken);
    }
}
