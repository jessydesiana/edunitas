package com.example.windows.edunitas;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by windows on 3/13/2018.
 */

public class Utility {
    String TAG = "Utility";
    public String getPostTime (long timeStamp){
        String postTime = "";
        Date dLastDate = null;

        TimeZone timeZone = TimeZone.getDefault();
        DateFormat objFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        objFormatter.setTimeZone(timeZone);

        Calendar objCalender = Calendar.getInstance();
        objCalender.setTimeInMillis(timeStamp);
        String result = objFormatter.format(objCalender.getTime());
        objCalender.clear();

        String dataPost = result;

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault());
        String dataCurrent = dateFormat.format(new Date());

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault());

        Date d1 = null;
        Date d2 = null;

        try{

            d1 = format.parse(dataPost);
            d2 = format.parse(dataCurrent);

            long diff = d2.getTime()-d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinute = diff / (60*1000) % 60;
            long diffHours = diff / (60*60*1000) % 24;
            long diffDays = diff / (24*60*60*1000);

            String firstDate = dataPost.substring(0, 10);
            String secondDate = dataCurrent.substring(0, 10);

            if (!firstDate.equals(secondDate)){
                SimpleDateFormat lastDateDf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                dLastDate = lastDateDf.parse(result);
                SimpleDateFormat dLastDateDf = new SimpleDateFormat("dd/MM/yyyy, HH:mm");
                postTime = dLastDateDf.format(dLastDate);
            }else if (diffDays < 1){
                if (diffHours > 0){
                    postTime = String.valueOf(diffHours) + " jam lalu";
                }else  if (diffMinute > 0){
                    postTime = String.valueOf(diffMinute) + " menit lalu";
                }else if (diffSeconds > 0){
                    postTime = String.valueOf(diffSeconds) + " detik lalu";
                }else if (diffDays == 0){
                    postTime = "1 detik lalu";
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(TAG, "error : " + e.toString());
        }
        return  postTime;

    }
}