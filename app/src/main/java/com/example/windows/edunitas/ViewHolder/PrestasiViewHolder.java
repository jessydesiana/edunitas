package com.example.windows.edunitas.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.windows.edunitas.Pojo.Post;
import com.example.windows.edunitas.Pojo.Prestasi;
import com.example.windows.edunitas.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

/**
 * Created by windows on 3/19/2018.
 */

public class PrestasiViewHolder extends RecyclerView.ViewHolder {
    public ImageView ivPiala;
    public TextView tvJuara, tvKeterangan;

    FirebaseAuth mAuth;
    FirebaseDatabase mDatabase;
    DatabaseReference mRootRef, mUserRef, mPostRef, mPretasiRef;
    FirebaseStorage mStorage;
    StorageReference storageReference;

    String TAG = "PrestasiViewHolder";

    Prestasi displayPrestasi = new Prestasi();
    public String prestasiKey;

    public PrestasiViewHolder(View itemView) {
        super(itemView);

        mStorage = FirebaseStorage.getInstance();
        storageReference = mStorage.getReference();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        mRootRef = mDatabase.getReference();
        mUserRef = mRootRef.child("users");
        mPostRef = mRootRef.child("posts");
        mPretasiRef = mRootRef.child("prestasi");

        ivPiala = itemView.findViewById(R.id.piala);
        tvJuara = itemView.findViewById(R.id.tvJuara);
        tvKeterangan = itemView.findViewById(R.id.tvKeterangan);

    }

    public void bindToPublic(final Prestasi prestasi, final String prestasiKey) {
        this.prestasiKey = prestasiKey;
        displayPrestasi = prestasi;

        ivPiala.setImageResource(R.drawable.piala);
        tvJuara.setText(prestasi.juara);
        tvKeterangan.setText(prestasi.keterangan);

    }
}
