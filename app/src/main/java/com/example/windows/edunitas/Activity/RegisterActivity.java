package com.example.windows.edunitas.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.windows.edunitas.Pojo.User;
import com.example.windows.edunitas.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class RegisterActivity extends AppCompatActivity {

    EditText etFullname, etEmail, etPassword, etSekolah;
    Button btnRegister;

    String uid;

    FirebaseAuth mAuth;
    FirebaseDatabase mDatabase;
    DatabaseReference mRootRef, mUserRef, mPostRef, mPrestasiRef;
    FirebaseStorage mStorage;
    StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mStorage = FirebaseStorage.getInstance();
        storageReference = mStorage.getReference();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        mRootRef = mDatabase.getReference();
        mUserRef = mRootRef.child("users");
        mPostRef = mRootRef.child("posts");
        mPrestasiRef = mRootRef.child("prestasi");

        etFullname = findViewById(R.id.registerEtFullname);
        etEmail = findViewById(R.id.registerEtEmail);
        etPassword = findViewById(R.id.registerEtPassword);
        btnRegister = findViewById(R.id.btnRegister);
        etSekolah = findViewById(R.id.registerEtSekolah);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fullname = etFullname.getText().toString();
                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();
                String sekolah = etSekolah.getText().toString();

                mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            Log.d("RegisterActivity", "BERHASIL REGISTER!");
                            uid = mAuth.getCurrentUser().getUid();
                        }
                    }
                });

                //mAuth.signInWithEmailAndPassword(email, password);
                //mAuth = FirebaseAuth.getInstance();
                User user = new User(fullname, email, password,"https://firebasestorage.googleapis.com/v0/b/edunitas-b04ba.appspot.com/o/image%2Fa0f47cd6-5550-4623-b6ea-b18e9321a3b1?alt=media&token=84a83ca6-6bb4-4cb0-b71a-a6fed18904fc", sekolah, uid);
                mUserRef.child(uid).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            startActivity(new Intent(RegisterActivity.this, BerandaActivity.class));
                            finish();
                        } else {
                            Log.d("RegisterActivity", task.getException().getLocalizedMessage());
                        }
                    }
                });

            }
        });
    }
}
