package com.example.windows.edunitas.Pojo;

import com.google.firebase.database.ServerValue;

/**
 * Created by windows on 3/25/2018.
 */

public class ProfilePost {
    public String content;
    public int sumLove;
    public int sumComment;
    public String photo;
    public Object timestamp;

    public ProfilePost() {
    }

    public ProfilePost(String content, int sumLove, int sumComment, String photo) {
        this.content = content;
        this.sumLove = sumLove;
        this.sumComment = sumComment;
        this.photo = photo;
        this.timestamp = ServerValue.TIMESTAMP;
    }
}
