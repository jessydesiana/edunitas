package com.example.windows.edunitas.ViewHolder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.windows.edunitas.Pojo.Message;
import com.example.windows.edunitas.Pojo.User;
import com.example.windows.edunitas.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by windows on 3/27/2018.
 */

public class RiwayatViewHolder extends RecyclerView.ViewHolder {

    CircleImageView photoProfileRiwayat;
    TextView tvNamaTeman, tvPesanTerakhir;

    private Context context;

    FirebaseAuth mAuth;
    FirebaseDatabase mDatabase;
    DatabaseReference mRootRef, mUserRef, mMessageRef;
    FirebaseStorage mStorage;
    StorageReference storageReference;

    Message displayMessage = new Message();
    public String messageKey;

    public RiwayatViewHolder(View itemView) {
        super(itemView);

        context = itemView.getContext();

        mStorage = FirebaseStorage.getInstance();
        storageReference = mStorage.getReference();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        mRootRef = mDatabase.getReference();
        mUserRef = mRootRef.child("users");
        mMessageRef = mRootRef.child("messages");

        photoProfileRiwayat = itemView.findViewById(R.id.profileRiwayatChat);
        tvNamaTeman = itemView.findViewById(R.id.tvNamaRiwayat);
        tvPesanTerakhir = itemView.findViewById(R.id.isiPesanTerakhir);

    }

    public void bindToRiwayat(final Message message, final String messageKey) {
        this.messageKey = messageKey;
        displayMessage = message;

        final String uid = mAuth.getCurrentUser().getUid();

        mUserRef.child(messageKey).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String namaTeman = dataSnapshot.child("fullName").getValue(String.class);
                String photoTeman = dataSnapshot.child("photoprofile").getValue(String.class);
                tvNamaTeman.setText(namaTeman);
                Picasso.with(itemView.getContext()).load(photoTeman).into(photoProfileRiwayat);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Query lastQuery = mMessageRef.child(uid).child(messageKey).limitToLast(1);
        Log.d("Riwayat Act" , "messageKey : " + messageKey);

        lastQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("Riwayat Act", "datasnapshot : " + dataSnapshot);
                final String key = dataSnapshot.getKey();
                String lastmassage = dataSnapshot.child("message").getValue(String.class);
                Log.d("Riwayat Act", "message terakhir : " + lastmassage);
                tvPesanTerakhir.setText(lastmassage);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

}
