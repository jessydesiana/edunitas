package com.example.windows.edunitas.Pojo;

import com.google.firebase.database.ServerValue;

/**
 * Created by windows on 3/25/2018.
 */

public class PrivatePost {

    public String fullname;
    public String content;
    public String photo;
    public long time;
    public Object timestamp;
    public int sumLove;
    public int sumComment;
    public String photoprofile;
    public String sekolahId;

    public PrivatePost() {
    }

    public PrivatePost(String fullname, String content, String photo, int sumLove, int sumComment, String photoprofile, String sekolahId) {
        this.fullname = fullname;
        this.content = content;
        this.photo = photo;
        this.sumLove = sumLove;
        this.sumComment = sumComment;
        this.timestamp = ServerValue.TIMESTAMP;
        this.photoprofile = photoprofile;
        this.sekolahId = sekolahId;
    }

}
