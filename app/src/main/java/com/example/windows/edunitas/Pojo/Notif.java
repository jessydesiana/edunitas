package com.example.windows.edunitas.Pojo;

/**
 * Created by windows on 4/7/2018.
 */

public class Notif {
    public String userId;
    public String token;
    public String pesan;

    public Notif() {
    }


    public Notif(String userId, String token, String pesan) {
        this.userId = userId;
        this.token = token;
        this.pesan = pesan;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }
}
