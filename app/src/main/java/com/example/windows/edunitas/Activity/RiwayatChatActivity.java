package com.example.windows.edunitas.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.windows.edunitas.Pojo.Message;
import com.example.windows.edunitas.Pojo.User;
import com.example.windows.edunitas.R;
import com.example.windows.edunitas.ViewHolder.FriendListViewHolder;
import com.example.windows.edunitas.ViewHolder.RiwayatViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class RiwayatChatActivity extends AppCompatActivity {

    private static final String TAG = "RiwayatChat Act";
    FirebaseAuth mAuth;
    FirebaseDatabase mDatabase;
    DatabaseReference mRootRef, mUserRef, mMessageRef;
    FirebaseStorage mStorage;
    StorageReference storageReference;

    RecyclerView mRecyclerView;
    FirebaseRecyclerAdapter<Message, RiwayatViewHolder> mAdapter;
    LinearLayoutManager mManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riwayat_chat);

        mStorage = FirebaseStorage.getInstance();
        storageReference = mStorage.getReference();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        mRootRef = mDatabase.getReference();
        mUserRef = mRootRef.child("users");
        mMessageRef = mRootRef.child("messages");

        mRecyclerView = findViewById(R.id.riwayat_recyclerview);
        mManager = new LinearLayoutManager(this.getApplicationContext());

        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);

        String uid = mAuth.getCurrentUser().getUid();
        mMessageRef.child(uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildren() != null) {
                    Log.d(TAG, "datasnapshot :" + dataSnapshot.getValue());
                    mRecyclerView.setLayoutManager(mManager);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        if (mDatabase == null) {
            mDatabase = FirebaseDatabase.getInstance();
        }
        mRootRef = mDatabase.getReference();
        mUserRef = mRootRef.child("users");
        mMessageRef = mRootRef.child("messages");

        mAuth = FirebaseAuth.getInstance();

        String uid = mAuth.getCurrentUser().getUid();

        mAdapter = new FirebaseRecyclerAdapter<Message, RiwayatViewHolder>(
                Message.class,
                R.layout.item_riwayat_chat,
                RiwayatViewHolder.class,
                mMessageRef.child(uid)
        ) {
            @Override
            protected void populateViewHolder(RiwayatViewHolder viewHolder, Message model, int position) {
                final DatabaseReference postRef = getRef(position);
                final String postKey = postRef.getKey();

                viewHolder.bindToRiwayat(model, postKey);
                Log.d(TAG,"Postkey : " + postKey);
            }
        };

        mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                Log.d(TAG, String.valueOf(itemCount));
                mRecyclerView.smoothScrollToPosition(mAdapter.getItemCount());
            }
        });

        mMessageRef.child(uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildren() != null) {
                    Log.d(TAG, "datasnapshot resume :" + dataSnapshot.getValue());
                    mRecyclerView.setLayoutManager(mManager);
                    mRecyclerView.setAdapter(mAdapter);
                    mRecyclerView.getRecycledViewPool().clear();
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

}
