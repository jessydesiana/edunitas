package com.example.windows.edunitas.ViewHolder;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.windows.edunitas.Activity.CommentActivity;
import com.example.windows.edunitas.Fragment.PublicFragment;
import com.example.windows.edunitas.Pojo.Post;
import com.example.windows.edunitas.R;
import com.example.windows.edunitas.Utility;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by windows on 3/13/2018.
 */

public class PublicViewHolder extends RecyclerView.ViewHolder {

    public CircleImageView profilePostPublic;
    public TextView namaPostPublic, waktuPostPublic, captionPostPublic, jumlahLovePublic, jumlahCommentPublic;
    public ImageView imagePostPublic, lovePostPublic, commentPostPublic;

    Boolean love = true;
    int jumlahLove;

    FirebaseAuth mAuth;
    FirebaseDatabase mDatabase;
    DatabaseReference mRootRef, mUserRef, mPostRef;
    FirebaseStorage mStorage;
    StorageReference storageReference;

    String TAG = "PublicViewHolder";

    Post displayPost = new Post();
    public String postKey;


    public PublicViewHolder(View itemView) {
        super(itemView);

        mStorage = FirebaseStorage.getInstance();
        storageReference = mStorage.getReference();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        mRootRef = mDatabase.getReference();
        mUserRef = mRootRef.child("users");
        mPostRef = mRootRef.child("posts");

        profilePostPublic = itemView.findViewById(R.id.profilePostPublic);
        namaPostPublic = itemView.findViewById(R.id.tvNamaPostPublic);
        waktuPostPublic = itemView.findViewById(R.id.tvWaktuPostPublic);
        captionPostPublic = itemView.findViewById(R.id.tvCaptionPostPublic);
        jumlahLovePublic = itemView.findViewById(R.id.jumlahLovePostPublic);
        jumlahCommentPublic = itemView.findViewById(R.id.jumlahCommentPostPublic);
        imagePostPublic = itemView.findViewById(R.id.ivPostPublic);
        lovePostPublic = itemView.findViewById(R.id.ivLovePostPublic);
        commentPostPublic = itemView.findViewById(R.id.ivCommentPostPublic);

    }

    public void bindToPublic(final Post post, final String postKey) {
        this.postKey = postKey;
        displayPost = post;

        Log.d(TAG, "content : " + post.content);
        Log.d(TAG, "love bindTopublic : " + love);
        captionPostPublic.setText(post.content);
        namaPostPublic.setText(post.fullname);
        jumlahLovePublic.setText(String.valueOf(post.sumLove));
        jumlahCommentPublic.setText(String.valueOf(post.sumComment));
        Picasso.with(itemView.getContext()).load(post.photo).into(imagePostPublic);
        if (post.photoprofile != null){
            Picasso.with(itemView.getContext()).load(post.photoprofile).into(profilePostPublic);
        } else {
            Picasso.with(itemView.getContext()).load(R.drawable.ic_action_person).into(profilePostPublic);
        }

        Utility util = new Utility();
        String postTime = util.getPostTime((Long) post.timestamp);
        waktuPostPublic.setText(postTime);

        lovePostPublic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                jumlahLove = post.sumLove;
                if (love == true){
                    lovePostPublic.setImageResource(R.drawable.ic_action_loveon);
                    jumlahLove = jumlahLove+1;
                    mPostRef.child("public").child(postKey).child("sumLove").setValue(jumlahLove);
                    jumlahLovePublic.setText(String.valueOf(jumlahLove));
                    love = false;
                    Log.d(TAG, "Love tambah satu " +love);
                } else{
                    lovePostPublic.setImageResource(R.drawable.ic_action_loveoff);
                    if ((jumlahLove-1) == 0){
                        jumlahLovePublic.setText(String.valueOf(0));
                        jumlahLove = 0;
                        mPostRef.child("public").child(postKey).child("sumLove").setValue(jumlahLove);
                        love = true;
                        Log.d(TAG, "Love nol " + love);
                    } else {
                        jumlahLove = jumlahLove - 1;
                        jumlahLovePublic.setText(String.valueOf(jumlahLove));
                        mPostRef.child("public").child(postKey).child("sumLove").setValue(jumlahLove);
                        love = true;
                        Log.d(TAG, "Love kurang satu " + love);
                    }
                }

            }
        });

        commentPostPublic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String status = "public";
                Context context = view.getContext();
                Intent intent = new Intent(context, CommentActivity.class);
                intent.putExtra("postKey", postKey);
                intent.putExtra("status", status);
//                intent.putExtra("fullname", post.fullname);
//                intent.putExtra("content", post.content);
//                intent.putExtra("photo", post.photo);
//                intent.putExtra("jumlahkomen", post.sumComment);
//                intent.putExtra("timestamp",post.timestamp);
                context.startActivity(intent);
            }
        });

    }
}
