package com.example.windows.edunitas.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.example.windows.edunitas.Pojo.Prestasi;
import com.example.windows.edunitas.Pojo.User;
import com.example.windows.edunitas.R;
import com.example.windows.edunitas.ViewHolder.FriendListViewHolder;
import com.example.windows.edunitas.ViewHolder.PrestasiViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import de.hdodenhof.circleimageview.CircleImageView;

public class FriendListActivity extends AppCompatActivity {

    private static final String TAG = "FriendList";
    FirebaseAuth mAuth;
    FirebaseDatabase mDatabase;
    DatabaseReference mRootRef, mUserRef, mPrestasiRef;
    FirebaseStorage mStorage;
    StorageReference storageReference;

    RecyclerView mRecyclerView;
    FirebaseRecyclerAdapter<User, FriendListViewHolder> mAdapter;
    LinearLayoutManager mManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_list);

        mStorage = FirebaseStorage.getInstance();
        storageReference = mStorage.getReference();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        mRootRef = mDatabase.getReference();
        mUserRef = mRootRef.child("users");
        mPrestasiRef = mRootRef.child("prestasi");

        mRecyclerView = findViewById(R.id.friendList_recyclerview);
        mManager = new LinearLayoutManager(this.getApplicationContext());

        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);

        mUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildren() != null) {
                    Log.d(TAG, "datasnapshot :" + dataSnapshot.getValue());
                    mRecyclerView.setLayoutManager(mManager);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mDatabase == null) {
            mDatabase = FirebaseDatabase.getInstance();
        }
        mRootRef = mDatabase.getReference();
        mUserRef = mRootRef.child("users");
        mPrestasiRef = mRootRef.child("prestasi");

        mAuth = FirebaseAuth.getInstance();

        mAdapter = new FirebaseRecyclerAdapter<User, FriendListViewHolder>(
                User.class,
                R.layout.item_friendlist,
                FriendListViewHolder.class,
                mUserRef
        ) {
            @Override
            protected void populateViewHolder(FriendListViewHolder viewHolder, User model, int position) {
                final DatabaseReference postRef = getRef(position);
                final String postKey = postRef.getKey();

                viewHolder.bindToUser(model, postKey);
                Log.d(TAG,"Postkey : " + postKey);
            }
        };

        mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                Log.d(TAG, String.valueOf(itemCount));
                mRecyclerView.smoothScrollToPosition(mAdapter.getItemCount());
            }
        });

        mUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildren() != null) {
                    Log.d(TAG, "datasnapshot resume :" + dataSnapshot.getValue());
                    mRecyclerView.setLayoutManager(mManager);
                    mRecyclerView.setAdapter(mAdapter);
                    mRecyclerView.getRecycledViewPool().clear();
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
