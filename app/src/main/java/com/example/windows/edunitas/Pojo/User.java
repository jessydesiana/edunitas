package com.example.windows.edunitas.Pojo;

/**
 * Created by windows on 3/23/2018.
 */

public class User {
    public String fullName;
    public String email;
    public String password;
    public String photoprofile;
    public String sekolah;
    public String uid;

    public User() {
    }

    public User(String fullName, String email, String password, String photoprofile, String sekolah, String uid) {
        this.fullName = fullName;
        this.email = email;
        this.password = password;
        this.photoprofile = photoprofile;
        this.sekolah = sekolah;
        this.uid = uid;
    }
}
