package com.example.windows.edunitas.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.windows.edunitas.Pojo.Message;
import com.example.windows.edunitas.Pojo.User;
import com.example.windows.edunitas.R;
import com.example.windows.edunitas.Utility;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

/**
 * Created by windows on 3/24/2018.
 */

public class MessageViewHolder extends RecyclerView.ViewHolder
{
    public TextView isiChat, waktuChat;
    RelativeLayout chatbubble;

    FirebaseAuth mAuth;
    FirebaseDatabase mDatabase;
    DatabaseReference mRootRef, mUserRef, mMessageRef;

    String userId;

    Message displayMessage = new Message();
    public String messageKey;

    public MessageViewHolder(View itemView) {
        super(itemView);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        mRootRef = mDatabase.getReference();
        mUserRef = mRootRef.child("users");
        mMessageRef = mRootRef.child("messages");

        userId = mAuth.getCurrentUser().getUid();

        isiChat = itemView.findViewById(R.id.isiChat);
        waktuChat = itemView.findViewById(R.id.waktuChat);
        chatbubble = itemView.findViewById(R.id.chatBubble);
    }

    public void bindToMessage(final Message message, final String messageKey) {
        this.messageKey = messageKey;
        displayMessage = message;

        Log.d("Message Act", "user Id : "+userId);
        Log.d("Message Act", "Friend Id : "+message.from);

        if ((message.from).equals(userId)){
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) chatbubble.getLayoutParams();
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

            chatbubble.setLayoutParams(params);
        } else {
            Log.d("Message Act", "Bukan chatmu");
        }

        isiChat.setText(message.message);
        Utility util = new Utility();
        String postTime = util.getPostTime((Long) message.timestamp);
        waktuChat.setText(postTime);

    }

}
