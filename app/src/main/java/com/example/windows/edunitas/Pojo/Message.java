package com.example.windows.edunitas.Pojo;

import com.google.firebase.database.ServerValue;

/**
 * Created by windows on 3/23/2018.
 */

public class Message {
    public String message;
    public String from;
    public Object timestamp;

    public Message() {
    }

    public Message(String message, String from) {
        this.message = message;
        this.from = from;
        this.timestamp = ServerValue.TIMESTAMP;
    }
}
