package com.example.windows.edunitas.ViewHolder;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.windows.edunitas.Activity.ChatRoomActivity;
import com.example.windows.edunitas.Pojo.Prestasi;
import com.example.windows.edunitas.Pojo.User;
import com.example.windows.edunitas.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by windows on 3/23/2018.
 */

public class FriendListViewHolder extends RecyclerView.ViewHolder {

    CircleImageView photoProfileFriend;
    TextView tvNameFriend, tvSekolah;

    CardView cvFriendList;

    private Context context;

    FirebaseAuth mAuth;
    FirebaseDatabase mDatabase;
    DatabaseReference mRootRef, mUserRef, mPretasiRef;
    FirebaseStorage mStorage;
    StorageReference storageReference;

    User displayUser = new User();
    public String userKey;

    public FriendListViewHolder(View itemView) {
        super(itemView);

        context = itemView.getContext();

        mStorage = FirebaseStorage.getInstance();
        storageReference = mStorage.getReference();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        mRootRef = mDatabase.getReference();
        mUserRef = mRootRef.child("users");
        mPretasiRef = mRootRef.child("prestasi");

        photoProfileFriend = itemView.findViewById(R.id.circlePhotoProfileFriendList);
        tvNameFriend = itemView.findViewById(R.id.nameFriendList);
        tvSekolah = itemView.findViewById(R.id.sekolahFriendList);
        cvFriendList = itemView.findViewById(R.id.cardViewFriendList);
    }
    public void bindToUser(final User user, final String userKey) {
        this.userKey = userKey;
        displayUser = user;

        if (!user.uid.equals(mAuth.getCurrentUser().getUid())){
            Picasso.with(itemView.getContext()).load(user.photoprofile).into(photoProfileFriend);
            tvNameFriend.setText(user.fullName);
            tvSekolah.setText(user.sekolah);
        } else {
            cvFriendList.setVisibility(View.GONE);
        }

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ChatRoomActivity.class);
                intent.putExtra("nama", user.fullName);
                intent.putExtra("photo", user.photoprofile);
                intent.putExtra("uid", user.uid);
                context.startActivity(intent);
            }
        });
    }
}
