package com.example.windows.edunitas.Activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.windows.edunitas.Fragment.PrivateFragment;
import com.example.windows.edunitas.Fragment.PublicFragment;
import com.example.windows.edunitas.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class BerandaActivity extends AppCompatActivity {

    FrameLayout frameLayout;
    TextView mTextMessage;

    FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener mAuthListener;

    PublicFragment publicFragment = new PublicFragment();
    PrivateFragment privateFragment = new PrivateFragment();

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigation_public:
                    fragmentTransaction.replace(R.id.content,publicFragment).commit();
                    getSupportActionBar().setTitle("Public");
                    return true;
                case R.id.navigation_private:
                    fragmentTransaction.replace(R.id.content,privateFragment).commit();
                    getSupportActionBar().setTitle("Private");
                    return true;
//                case R.id.navigation_notifications:
//                    mTextMessage.setText(R.string.title_notifications);
//                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beranda);
//        String uid = mAuth.getCurrentUser().getUid();
//        Log.d("BerandaActivity", uid);

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                Log.d("BerandaActivity", "user :"+user);
                if (user != null){
                    Log.d("BerandaActivity", "user is : login");
                }else {
                    Log.d("BerandaActivity", "user is : logOut");
                    Intent intent = new Intent(BerandaActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        };

//        mAuth = FirebaseAuth.getInstance();
//        if (mAuth == null){
//            startActivity(new Intent(BerandaActivity.this ,LoginActivity.class));
//            finish();
//        } else {
//            String uid = mAuth.getCurrentUser().getUid();
//            Log.d("BerandaActivity", uid);
//        }

        frameLayout = (FrameLayout) findViewById(R.id.content);
        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content,publicFragment).commit();
        getSupportActionBar().setTitle("Public");
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        mAuth.addAuthStateListener(mAuthListener);
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        if (mAuthListener != null){
//            mAuth.removeAuthStateListener(mAuthListener);
//        }
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.beranda,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.tambahPost){
            startActivity(new Intent(BerandaActivity.this, MainActivity.class));
        } else if (id == R.id.profile){
            startActivity(new Intent(BerandaActivity.this, ProfileActivity.class));
        } else if (id == R.id.setting){
            startActivity(new Intent(BerandaActivity.this, SettingActivity.class));
        } else if (id == R.id.friends){
            startActivity(new Intent(BerandaActivity.this, FriendListActivity.class));
        } else if (id == R.id.signOut) {
            mAuth.signOut();
            startActivity(new Intent(BerandaActivity.this, LoginActivity.class));
        } else if (id == R.id.riwayatChat){
            startActivity(new Intent(BerandaActivity.this, RiwayatChatActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }
}
